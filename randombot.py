#!/usr/bin/python2.7

import numpy as np
import sys
from random import random

mynum = int(sys.argv[1])

world   = np.loadtxt ('world'  ).astype(int)
players = np.loadtxt ('players').astype(int)

(myx,myy) = np.where(players==mynum)
myx = myx[0]
myy = myy[0]

r = 0
for _ in range(100):
	r = int(random() * 4)
	try:
		if r == 0:
			if myx - 1 < 0 : continue
			if world[myx-1, myy] == 0: break
		elif r == 1:
			if myy -1 < 0 : continue
			if world[myx, myy-1] == 0: break
		elif r == 2:
			if world[myx+1, myy] == 0: break
		elif r == 3:
			if world[myx, myy+1] == 0: break
	except:
		pass

print r
