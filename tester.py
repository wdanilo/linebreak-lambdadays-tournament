from matplotlib import pyplot
import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import sys
from random import random
from operator import itemgetter
import subprocess
import time
import sys


def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

class Timeout(Exception):
    pass

def run(command, timeout=10):
    proc = subprocess.Popen(command, bufsize=0, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    poll_seconds = .01
    deadline = time.time()+timeout
    while time.time() < deadline and proc.poll() == None:
        time.sleep(poll_seconds)

    if proc.poll() == None:
        if float(sys.version[:3]) >= 2.6:
            proc.terminate()
        raise Timeout()

    stdout, stderr = proc.communicate()
    return stdout, stderr, proc.returncode


def extendTo(s, i):
    return (s + " " * (i - len(s)))

class Player:
    def __init__(self, x, y, num, prog, color):
        self.x     = x
        self.y     = y
        self.num   = num
        self.prog  = prog
        self.color = color

    def __str__(self):
        return "(Player %s [%s] (%s, %s))" % (self.prog, self.color, self.x, self.y)

class Step:
    def __init__(self):
        self.points = 0

def kill(step, player):
    playerStatus[player] = Status.DEAD
    step.points += 1


# max 6 players!
colors = ['black', '#990000', 'green', '#ff9900', '#cc00cc', 'blue', '0.7', 'red', '#00cc00', 'yellow', 'magenta', 'cyan', 'white']

cmap = mpl.colors.ListedColormap(colors)

Status = enum('LIVE', 'DEAD')


bounds=[i/float(cmap.N+1) for i in range(cmap.N+1)]
norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

fig = plt.figure()

if float(sys.version[:3]) >= 3.0:
    print ("===========================")
    print ("PLEASE USE PYTHON 2.7. It may work with older Pythons also!")
    print ("===========================")
    sys.exit()

if len(sys.argv) < 3:
    print ("===========================")
    print ("USAGE: %s WIDTH HEIGHT [PROGRAMS]" % sys.argv[0])
    print ("===========================")
    sys.exit()


runTillTheEnd = False
optionOffset  = 0

if sys.argv[1] == '-x':
    runTillTheEnd = True
    optionOffset = 1

width = int(sys.argv[1 + optionOffset])
height = int(sys.argv[2 + optionOffset])
progs = sys.argv[3 + optionOffset:]

num_players = len(progs)

world   = np.zeros(shape=(width, height))
players = np.zeros(shape=(width, height))
playerStatus = [Status.LIVE] * num_players
playerPoints = [0] * num_players

for player in range(num_players):
    for _ in range(1000):
        x = int(random() * width)
        y = int(random() * height)
        if players[x,y] == 0:
            players[x,y] = player + 1
            break

np.savetxt ('world', world, fmt = "%d")
np.savetxt ('players', players, fmt = "%d")

end = False
ims = []

def getPlayer(players, num):
    colorNum = num + 1
    (playerx,playery) = np.where(players == colorNum)
    playerx = playerx[0] if len(playerx) > 0 else 0
    playery = playery[0] if len(playery) > 0 else 0
    prog   = progs[num]
    color  = colors[colorNum]
    player = Player(playerx, playery, num, prog, color)
    return player

for i in range(1000000):
    if end: break

    liveNum = 0
    for player in range(num_players):
        if playerStatus[player] == Status.LIVE:
            liveNum += 1

    print ("frame %s (live players: %s)" % (i, liveNum))


    world      = np.loadtxt ('world'  ).astype(int)
    players    = np.loadtxt ('players').astype(int)
    newplayers = np.zeros(shape=(width, height))
    heads      = np.zeros(shape=(width, height))
    world     += players
    step       = Step()

    for playerNum in range(num_players):
        if playerStatus[playerNum] == Status.DEAD:
            continue

        colorNum = playerNum + 1
        # (playerx,playery) = np.where(players == colorNum)
        prog  = progs[playerNum]
        color = colors[colorNum]
        player = getPlayer(players, playerNum)

        # make the payer leave no trail with a certain chance
        if random() < 0.1:
            world[player.x, player.y] = 0

        (move,_,_) = run([prog, str(colorNum)], timeout = 0.5)
        try:
            move = int(move)
        except:
            print ("%s provided an invalid move: '%s'" % (player, move))
            kill(step, player.num)
            continue


        if move == 0: player.x -= 1
        elif move == 1: player.y -= 1
        elif move == 2: player.x += 1
        elif move == 3: player.y += 1
        # if move == 0: player.y -= 1 # this is the fixed (untranslated) matrix, because Wojtek wrote it as x representing the row and y the column :D
        # elif move == 1: player.x -= 1
        # elif move == 2: player.y += 1
        # elif move == 3: player.x += 1
        else: kill(step, player.num) # ... make sure this works

        if (player.x < 0) or (player.y < 0) or (player.x >= width) or (player.y >= height):
            print ("%s crashed into wall" % player)
            kill(step, player.num)
            continue

        worldVal = world[player.x, player.y]
        if worldVal != 0:
            otherPlayer = getPlayer (players, worldVal-1)
            print ("%s crashed into tail of %s" % (player, otherPlayer))
            kill(step, player.num)
            continue

        playersVal = int(newplayers[player.x, player.y])
        if playersVal != 0:
            otherPlayer = getPlayer (players, playersVal-1)
            kill(step, player.num)
            kill(step, otherPlayer.num)
            print ("%s and %s had head-on collision" % (player, otherPlayer))
            continue

        newplayers[player.x, player.y] = colorNum
        heads[player.x, player.y] = colorNum + 6

    end = True
    alivePlayers = 0
    for player in range(num_players):
        if playerStatus[player] == Status.LIVE:
            playerPoints[player] += step.points
            alivePlayers += 1

    if alivePlayers > 1 or (alivePlayers > 0 and runTillTheEnd):
        end = False

    if end:
        print ("END OF THE GAME!")
        x = zip (progs, colors[1:], playerPoints)
        x.sort(key=itemgetter(2))
        for (prog, color, points) in reversed(x):
            print ("%s [%s] : %s" %(extendTo(prog, 20), extendTo(color, 7), points))


    np.savetxt ('world', world, fmt = "%d")
    np.savetxt ('players', newplayers, fmt = "%d")

    # zvals = np.copy (world)
    # zvals += players
    zvals = (world + heads) / float(cmap.N)

    im = pyplot.imshow(zvals,interpolation='nearest', cmap = cmap, norm=norm)
    ims.append([im])

ani = animation.ArtistAnimation(fig, ims, interval=100, blit=True, repeat_delay=1000)

plt.show()
